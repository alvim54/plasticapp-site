import React, { Component, Fragment } from 'react'
import { Link, navigate } from 'gatsby'
import { Grid, Cell, Wrapper, Button, Dropdown, Responsive, Blanket } from 'cinnecta-ui'
import Icon from '../components/Icon'
import PageHeader from './PageHeader'
import { withStyles } from '../helpers/withStyles'
import logo from '../images/logo.png'
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock'
import ContactForm from './ContactForm'

class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      contactIsVisible: false,
      menuIsOpen: false
    }
  }

  showContact() {
    disableBodyScroll(this.contact)
    this.setState({ contactIsVisible: true })
  }

  hideContact() {
    enableBodyScroll(this.contact)
    this.setState({ contactIsVisible: false })

  }

  showMenu() {
    disableBodyScroll(this.menu)
    this.setState({ menuIsOpen: true })
  }

  hideMenu() {
    enableBodyScroll(this.menu)
    this.setState({ menuIsOpen: false })

  }

  render() {
    const { backgroundColor, css, placeholder, styles, withBackground } = this.props
    const { contactIsVisible, menuIsOpen } = this.state

    return (
      <Fragment>
        <div ref={ref => this.contact = ref} {...css(styles.contactContainer, contactIsVisible && styles.contactContainer_visible)}>
          <Wrapper container>
            <Grid>
              <Cell size={12} largeSize={8} largeSpacing>
                <div {...css(styles.contactCloseButtonContainer)}>
                  <Button inverse small onClick={() => this.hideContact()}>fechar X</Button>
                </div>
              </Cell>
            </Grid>
          </Wrapper>
          <ContactForm/>
        </div>
        <PageHeader placeholder={placeholder} backgroundColor={backgroundColor} withBackground={withBackground}>
          <Wrapper container>
            <div {...css(styles.headerContainer)}>
              <Grid resetSpacing center>
                <Responsive hideFor="large">
                  <Cell shrink noSpacing>
                    <Button transparent onClick={() => this.showMenu()}>oi</Button>
                  </Cell>
                </Responsive>
                <Cell shrink>
                  <Link to="/">
                    <img {...css(styles.logo)} src={logo} alt="PlasticApp logo"/>
                  </Link>
                </Cell>
                <Cell auto largeSpacing>
                  <div {...css(styles.menu, menuIsOpen && styles.menu_visible)} ref={ref => this.menu = ref}>
                    <Dropdown
                      button
                      transparent
                      compact
                      left
                      noSpacing
                      onSelectOption={option => navigate(option.value)}
                      options={[
                        { label: 'timeline', value: '/#timeline' },
                        { label: 'chat online', value: '/#chat-online' },
                        { label: 'analytics', value: '/#analytics' },
                        { label: 'pagamento online', value: '/#pagamento-online' },
                        { label: 'marketing digital', value: '/#marketing-digital' },
                        { label: 'assistência continuada', value: '/#assistencia-continuada' },
                        { label: 'segurança de dados', value: '/#seguranca-de-dados' },
                      ]}
                      label="soluções"
                    />
                    <Button transparent compact noSpacing onClick={() => navigate('/como-funciona')}>como funciona?</Button>
                    <Button transparent compact noSpacing onClick={() => navigate('/plano')}>plano</Button>
                    <Dropdown
                      button
                      transparent
                      compact
                      left
                      noSpacing
                      onSelectOption={option => navigate(option.value)}
                      options={[
                        { label: 'blog', value: '#' },
                        { label: 'e-books', value: '/e-books' },
                      ]}
                      label="conteúdo"
                    />
                    <Button transparent compact noSpacing onClick={() => this.showContact()}>contato</Button>
                  </div>
                </Cell>
                <Cell shrink>
                  <Button small inverse noSpacing>entrar</Button>
                  <Responsive showFor="large">
                    <Button small primary noSpacing>comece agora</Button>
                  </Responsive>
                </Cell>
              </Grid>
            </div>
          </Wrapper>
          <Blanket
            visible={menuIsOpen}
            hideFor="large"
            onClick={() => this.hideMenu()}
            onClose={() => {}}
          />
        </PageHeader>
      </Fragment>
    )
  }
}

export default withStyles(({ breakpoint, color, units }) => ({
  headerContainer: {
    paddingTop: units(1.25),
    paddingBottom: units(1.25),
    height: 90,
    boxSizing: 'border-box',

    [breakpoint.large]: {
      height: 108,
    }
  },

  contactContainer: {
    position: 'fixed',
    top: 0,
    right: 0,
    left: 0,
    height: '100vh',
    overflow: 'auto',
    background: color.grayLight,
    transform: 'translateY(-100%)',
    transition: 'transform .2s',
    zIndex: 10
  },

  menu: {
    position: 'fixed',
    right: 0,
    top: 0,
    height: '100vh',
    background: color.grayLighter,
    paddingTop: 108,
    paddingLeft: units(1.25),
    paddingRight: units(3),
    display: 'flex',
    flexDirection: 'column',
    transform: 'translateX(100%)',
    transition: 'transform .2s',
    zIndex: 1000,

    [breakpoint.large]: {
      position: 'static',
      flexDirection: 'row',
      transform: 'unset',
      padding: 0,
      background: color.transparent,
      height: 'unset'
    }
  },

  menu_visible: {
    transform: 'unset'
  },

  contactContainer_visible: {
    transform: 'unset'
  },

  contactCloseButtonContainer: {
    textAlign: 'right',
    paddingTop: units(2)
  },

  logo: {
    maxWidth: units(9),

    [breakpoint.large]: {
      maxWidth: units(12)
    }
  }
}))(Header)
