import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon';
import { withStyles } from '../helpers/withStyles';
import { withStylesPropTypes } from 'react-with-styles';

const panelPropTypes = {
  ...withStylesPropTypes,
  panelKey: PropTypes.string,
  borderless: PropTypes.bool,
  borderColor: PropTypes.string,
};

const panelHeaderPropTypes = {
  ...withStylesPropTypes,
  title: PropTypes.string,
  icon: PropTypes.string,
  backgroungColor: PropTypes.string,
  color: PropTypes.string
};

const AccordionContext = React.createContext({
  activePanel: null,
  setActivePanel: () => {}
});

class Accordion extends Component {
  constructor(props) {
    super(props);

    this.setActivePanel = panel => this.setState({ activePanel: panel });

    this.state = {
      activePanel: null,
      setActivePanel: this.setActivePanel
    }
  }

  render() {
    const { css, styles, children } = this.props;

    return (
      <div {...css(styles.accordion)}>
        <AccordionContext.Provider value={this.state}>
          { children }
        </AccordionContext.Provider>
      </div>
    );
  }
}

const AccordionPanel = ({ css, styles, borderless, panelKey, children }) => (
  <AccordionContext.Consumer>
    {({ activePanel }) => {
      const active = activePanel === panelKey;

      const childrenWithProps = children ? React.Children.map(children, child => React.cloneElement(child, { panelKey, active })) : children;

      return (
        <div
          {...css(
            styles.accordion__panel,
            borderless && styles.accordion__panel__bordeless
          )}
        >
          { childrenWithProps }
        </div>
      );
    }}
  </AccordionContext.Consumer>
)

const AccordionPanelHeader = ({ css, styles, panelKey, active, title, icon, linkTo }) => (
  <AccordionContext.Consumer>
    {({ setActivePanel }) => (
      <div {...css(styles.accordion__panel__header)} onClick={() => setActivePanel(active ? null : panelKey)}>
        {icon &&
        <div {...css(styles.accordion__panel__header__icon)}>
          <Icon name={icon}/>
        </div>
        }
        {linkTo && <a href={linkTo}></a>}
        { title }
        <svg
          {...css(
            styles.accordion__panel__header__expandIcon,
            !active && styles.accordion__panel__header__collapsedIcon
          )}
          x="0px"
          y="0px"
          viewBox="0 0 477.2 477.2"
          xmlns="http://www.w3.org/2000/svg"
        >
          <g transform="matrix(-1, 0, 0, -1, 477.099976, 478.5)">
            <path d="M 74.15 291.85 C 74.15 295.95 75.85 300.05 78.75 302.95 L 123.35 347.55 C 126.25 350.45 130.35 352.15 134.45 352.15 C 138.55 352.15 142.65 350.45 145.55 347.55 L 238.45 254.65 L 331.35 347.55 C 334.35 350.55 338.55 352.25 342.75 352.15 C 346.75 352.05 350.75 350.35 353.65 347.55 L 398.25 302.95 C 401.15 300.05 402.85 295.95 402.85 291.85 C 402.85 287.75 401.15 283.65 398.25 280.75 L 249.65 132.15 C 246.75 129.25 242.65 127.55 238.55 127.55 C 234.45 127.55 230.35 129.25 227.45 132.15 L 78.85 280.75 C 75.85 283.65 74.15 287.75 74.15 291.85 L 74.15 291.85 Z" />
          </g>
        </svg>
      </div>
    )}
  </AccordionContext.Consumer>
)

const AccordionPanelContent = ({ css, styles, children, active }) => (
  <div
    {...css(
      styles.accordion__content,
      !active && styles.accordion__panel__content__collapsed
    )}
  >
    { children }
  </div>
)

AccordionPanel.propTypes = panelPropTypes;
AccordionPanelHeader.propTypes = panelHeaderPropTypes;

const AccordionWithStyles = withStyles(({ units }) =>({
  accordion: {
    display: 'flex',
    flexDirection: 'column'
  },
}))(Accordion);

const AccordionPanelWithStyles = withStyles(({ color, units, radius }) =>({
  accordion__panel: {
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#ccc'
  },

  accordion__panel__bordeless: {
    border: 'none'
  }
}))(AccordionPanel);

const AccordionPanelHeaderWithStyles = withStyles(({ color, units, radius }) =>({
  accordion__panel__header: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: units(0.5),
    border: 'none',
    cursor: 'pointer',

    /*':after': {
      content: '',
      width: units(1),
      height: units(1),
      display: 'inline-block',
      position: 'absolute',
      right: 0,
      background: 'url("./resources/images/ico-arrow.svg") no-repeat',
      transform: 'rotate(180deg)',
      transition: 'transform 0.3s'
    },*/

    ':hover': {
      background: color.secondary
    }
  },

  accordion__panel__header__icon: {
    marginRight: units(0.5)
  },

  accordion__panel__header__expandIcon: {
    position: 'absolute',
    right: units(1),
    width: units(0.7),
    fill: color.gray,
    transform: 'rotate(180deg)',
    transition: 'transform 0.3s'
  },

  accordion__panel__header__collapsedIcon: {
    transform: 'rotate(0deg)'
  },
}))(AccordionPanelHeader);

const AccordionPanelContentWithStyles = withStyles(({ color, units, radius }) =>({
  accordion__panel__content: {
    height: 'auto',
    opacity: 1,
    overflowY: 'hidden',
    overflowX: 'auto',
    transition: 'all 0.2s',
    padding: units(1)
  },

  accordion__panel__content__collapsed: {
    height: 0,
    opacity: 0,
    transition: 'all 0.2s'
  }
}))(AccordionPanelContent);

AccordionWithStyles.Panel = AccordionPanelWithStyles;
AccordionWithStyles.PanelHeader = AccordionPanelHeaderWithStyles;
AccordionWithStyles.PanelContent = AccordionPanelContentWithStyles;

export default AccordionWithStyles;
