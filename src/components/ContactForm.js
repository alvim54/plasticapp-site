import React, { Component } from 'react'
import { Grid, Cell, Wrapper, Input, Title, Text, Spacing, TextArea, Anchor, Button } from 'cinnecta-ui'
import { withStyles } from '../helpers/withStyles'

class ContactForm extends Component {
  render() {
    return (
      <div>
        <Wrapper container>
          <Spacing vertical={4}>
            <Grid>
              <Cell largeSpacing size={12} mediumSize={8}>
                <Grid resetSpacing>
                  <Cell size={12}>
                    <Spacing bottom={2}>
                      <Title mono level={3}>Fale conosco</Title>
                      <Text small>Mande sua mensagem e um de nossos consultores entrará em contato em breve</Text>
                    </Spacing>
                  </Cell>
                  <Cell size={12} largeSize={6}>
                    <Input label="nome:" name="nome" />
                  </Cell>
                  <Cell size={12} largeSize={6}>
                    <Input label="empresa:" name="empresa" />
                  </Cell>
                  <Cell size={12} largeSize={6}>
                    <Input type="email" label="email:" name="email" />
                  </Cell>
                  <Cell size={12} largeSize={6}>
                    <Input type="tel" label="telefone:" name="telefone" />
                  </Cell>
                  <Cell size={12}>
                    <TextArea label="mensagem:" name="mensagem" minRows={10} />
                    <Spacing vertical={2}>
                      <Button large tall wide>enviar</Button>
                    </Spacing>
                  </Cell>
                </Grid>
              </Cell>
              <Cell largeSpacing size={12} mediumSize={4}>
                <Title level={4} large bold mono>contato</Title>
                <Spacing bottom={2}>
                  <Text small>31 3456 7890</Text>
                  <Text small>31 98765 4321</Text>
                </Spacing>
                <Anchor external href="mailto:contato@plasticapp.com.br">contato@plasticapp.com.br</Anchor>
              </Cell>
            </Grid>
          </Spacing>
        </Wrapper>
      </div>
    )
  }
}

export default withStyles(({ color }) => ({
  form: {

  }
}))(ContactForm)

