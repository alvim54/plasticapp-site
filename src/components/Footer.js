import React from 'react'
import { navigate } from 'gatsby'
import { Wrapper, Grid, Cell, Text } from 'cinnecta-ui'
import { withStyles } from '../helpers/withStyles'
import logo from '../images/logo.png'

const Footer = ({ css, styles }) => (
  <div {...css(styles.container)}>
    <Wrapper container>
      <Grid>
        <Cell size={12} largeSize={3}>
          <img {...css(styles.logo)} src={logo} alt="PlasticApp logo" />
        </Cell>
        <Cell size={12} mediumSize={6} largeSize={3}>
          <div {...css(styles.menu)}>
            <Text small bold>soluções</Text>
            <div {...css(styles.link)} onClick={() => navigate('/#timeline')}><Text mono>Timeline</Text></div>
            <div {...css(styles.link)} onClick={() => navigate('/#chat-online')}><Text mono>Chat online</Text></div>
            <div {...css(styles.link)} onClick={() => navigate('/#analytics')}><Text mono>Analytics</Text></div>
            <div {...css(styles.link)} onClick={() => navigate('/#pagamento-online')}><Text mono>Pagamento online</Text></div>
            <div {...css(styles.link)} onClick={() => navigate('/#marketing-digital')}><Text mono>Marketing digital</Text></div>
            <div {...css(styles.link)} onClick={() => navigate('/#assistencia-continuada')}><Text mono>Assistência continuada</Text></div>
            <div {...css(styles.link)} onClick={() => navigate('/#seguranca-de-dados')}><Text mono>Segurança de dados</Text></div>
          </div>
        </Cell>
        <Cell size={12} mediumSize={6} largeSize={3}>
          <div {...css(styles.menu)}>
            <Text small bold>conteúdo</Text>
            <div {...css(styles.link)} onClick={() => navigate('#')}><Text mono>Blog</Text></div>
            <div {...css(styles.link)} onClick={() => navigate('/e-books')}><Text mono>E-books</Text></div>
          </div>
          <div {...css(styles.menu)}>
            <Text small bold>plasticapp</Text>
            <div {...css(styles.link)} onClick={() => navigate('/plano')}><Text mono>Plano</Text></div>
            <div {...css(styles.link)} onClick={() => navigate('/como-funciona')}><Text mono>Como funciona?</Text></div>
            <div {...css(styles.link)} onClick={() => navigate('#')}><Text mono>Trabalhe conosco</Text></div>
          </div>
        </Cell>
        <Cell size={12} mediumSize={6} largeSize={3}>
          <div {...css(styles.menu)}>
            <Text small bold>contato</Text>
            <div {...css(styles.text)}><Text mono small>31 3456 7890</Text></div>
            <div {...css(styles.text)}><Text mono small>31 98765 4321</Text></div>
          </div>
          <div {...css(styles.menu)}>
            <a {...css(styles.link)} href="mailto:contato@plasticapp.com.br"><Text mono>contato@plasticapp.com.br</Text></a>
          </div>
        </Cell>
      </Grid>
    </Wrapper>
  </div>
)

export default withStyles(({ color, units }) => ({
  container: {
    background: color.grayLighter,
    paddingTop: 80,
    paddingBottom: 140
  },

  logo: {
    width: 180,
    marginBottom: 40
  },

  text: {
    marginTop: units(1),
  },

  link: {
    cursor: 'pointer',
    marginTop: units(1),
    textDecoration: 'none',
    color: color.grayDarker,

    ':hover': {
      textDecoration: 'underline'
    }
  },

  menu: {
    marginBottom: 60
  }
}))(Footer)
