import React from 'react'
import { withStyles } from '../helpers/withStyles'
import icons from '../images/icons.svg'
import PropTypes from 'prop-types'
import { withStylesPropTypes } from 'react-with-styles'

const url = 'https://file.myfontastic.com/a8zw8XTvUHKCNmQzqjYEQD/sprites/1542489882.svg'
const propTypes = {
  ...withStylesPropTypes,
  huge: PropTypes.bool,
  inverse: PropTypes.bool,
  large: PropTypes.bool,
  micro: PropTypes.bool,
  mono: PropTypes.bool,
  muted: PropTypes.bool,
  name: PropTypes.oneOf([
    'auto-location',
    'check',
    'delete',
    'error',
    'filter',
    'import-location',
    'location',
    'more',
    'search',
    'settings',
    'vector',
  ]),
  noPadding: PropTypes.bool,
  primary: PropTypes.bool,
  secondary: PropTypes.bool,
  small: PropTypes.bool,
  smaller: PropTypes.bool,
  text: PropTypes.bool,
}

const Icon = ({ css, huge, inverse, large, micro, mono, muted, name, noPadding, primary, secondary, small, smaller, styles, text }) => {

  return (
    <div {...css(styles.icon, noPadding && styles.icon_noPadding)}>
      <svg
        {...css(
          styles.icon__svg,
          huge && styles.icon__svg_huge,
          inverse && styles.icon__svg_inverse,
          large && styles.icon__svg_large,
          micro && styles.icon__svg_micro,
          mono && styles.icon__svg_mono,
          muted && styles.icon__svg_muted,
          primary && styles.icon__svg_primary,
          secondary && styles.icon__svg_secondary,
          small && styles.icon__svg_small,
          smaller && styles.icon__svg_smaller,
          text && styles.icon__svg_text
        )}
        viewBox='0 0 16 16'>
          <use xlinkHref={`${url}#icon-${name}`} />
      </svg>
    </div>
  )
};

Icon.propTypes = propTypes;

const IconWithStyles = withStyles(({ color, icon }) => ({
  icon: {
    padding: 2,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },

  icon_noPadding: {
    padding: 0
  },

  icon__svg: {
    fill: icon.defaultColor,
    width: icon.size.default
  },

  icon__svg_muted: {
    fill: color.blueGrayDark,
  },

  icon__svg_primary: {
    fill: color.primary,
  },

  icon__svg_secondary: {
    fill: color.secondary,
  },

  icon__svg_inverse: {
    fill: color.white,
  },

  icon__svg_mono: {
    fill: color.black,
  },

  icon__svg_text: {
    fill: color.grayDarker,
  },

  icon__svg_micro: {
    width: icon.size.micro,
  },

  icon__svg_small: {
    width: icon.size.small,
  },

  icon__svg_smaller: {
    width: icon.size.smaller,
  },

  icon__svg_large: {
    width: icon.size.large,
  },

  icon__svg_huge: {
    width: icon.size.huge,
  },
}))(Icon);

export default IconWithStyles
