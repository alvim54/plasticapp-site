import React from 'react'
import { Image, Text, Spacing } from 'cinnecta-ui'
import { withStyles } from '../helpers/withStyles'

const Card = ({ css, title, image, description, styles }) => (
  <div {...css(styles.container)}>
    <Image source={image} alt="imagem do card" />
    <Spacing inside vertical={1}>
      <Text small primary>{ title }</Text>
      <Text micro>{ description }</Text>
    </Spacing>
  </div>
)

export default withStyles(({ color, units, radius }) => ({
  container: {
    borderRadius: radius,
    cursor: 'pointer',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: color.grayLighter,
    padding: units(1),
    marginBottom: units(2)
  }
}))(Card)
