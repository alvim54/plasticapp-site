import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'
import { ResponsiveWrapper } from 'cinnecta-ui'

import Header from './header'
import Footer from './Footer'
import './layout.css'

const Layout = ({ children, headerPlaceholder }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Helmet
          title={data.site.siteMetadata.title}
          meta={[
            { name: 'description', content: 'Plasticapp' },
            { name: 'keywords', content: 'plastic, app' },
          ]}
        >
          <html lang="en" />
        </Helmet>
        <ResponsiveWrapper>
          <Header
            siteTitle={data.site.siteMetadata.title}
            placeholder={headerPlaceholder}
          />
          <div>
            {children}
          </div>
          <Footer/>
        </ResponsiveWrapper>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  headerPlaceholder: PropTypes.bool
}

export default Layout
