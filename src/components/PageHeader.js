import React, { Component, Fragment } from 'react'
import _ from 'lodash'
import { withStyles } from '../helpers/withStyles'

const defaultProps = {
  backgroundColor: 'rgba(255,255,255,.95)',
  placeholder: false,
  delta: 20
}

class PageHeader extends Component {
  constructor(props) {
    super(props)
    this.state = {
      lastScrollTop: 0,
      visible: true,
      headerHeight: 0,
      showBackground: false
    }

    this.handleScroll = this.handleScroll.bind(this)
    this.hasScrolled = _.throttle(this.hasScrolled, 250)
    this.header = null
    this.setHeaderRef = header => {
      this.header = header
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
    this.setState({ headerHeight: this.header.clientHeight})

  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  hideHeader() {
    this.setState({ visible: false })
  }

  showHeader() {
    this.setState({ visible: true })
  }

  getPageHeight() {
    return Math.max(
      document.body.scrollHeight, document.documentElement.scrollHeight,
      document.body.offsetHeight, document.documentElement.offsetHeight,
      document.body.clientHeight, document.documentElement.clientHeight
    )
  }

  hasScrolled() {
    const st = window.scrollY
    const navbarHeight = this.header.offsetHeight
    const { delta } = this.props

    if(st > delta + navbarHeight) {
      this.setState({ showBackground: true })
    } else {
      this.setState({ showBackground: false })
    }

    if(Math.abs(this.state.lastScrollTop - st) <= delta) {
      return
    }

    if (st > this.state.lastScrollTop && st > navbarHeight){
      this.hideHeader()
    } else if(st < this.getPageHeight()) {
      this.showHeader()
    }
    this.setState({ lastScrollTop: st })

  }

  handleScroll(event) {
    this.hasScrolled()
  }

  render() {
    const { backgroundColor, children, css, placeholder, withBackground, styles } = this.props
    const { visible, headerHeight, showBackground } = this.state

    return (
      <Fragment>
        <header
          ref={ref => this.setHeaderRef(ref)}
          {...css(
            styles.header,
            !visible && styles.header_hidden,
            (showBackground || withBackground) && { background: backgroundColor }
          )}
        >
          { children }
        </header>
        { placeholder && <div {...css(styles.placeholder)} style={{ height: headerHeight }} /> }
      </Fragment>
    )
  }
}

PageHeader.defaultProps = defaultProps

export default withStyles(({ color }) => ({
  header: {
    position: 'fixed',
    top: 0,
    transition: 'transform 0.1s ease-in-out, background 0.1s ease-in-out',
    width: '100%',
    boxSizing: 'border-box',
    zIndex: 9
  },

  header_hidden: {
    transform: 'translateY(-100%)'
  },

  placeholder: {
    width: '100vw'
  }
}))(PageHeader)
