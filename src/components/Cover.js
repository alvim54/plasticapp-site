import React from 'react'
import { navigate } from 'gatsby'
import { withStyles } from '../helpers/withStyles'
import coverImage from '../images/cover.jpg'
import { Grid, Cell, Wrapper, Title, Button, Spacing, Responsive } from 'cinnecta-ui'

const Cover = ({ children, css, styles }) => (
  <div {...css(styles.cover)}>
    <Wrapper container fullHeight>
      <Grid fullHeight>
        <Responsive showFor="medium">
          <Cell fullHeight noSpacing size={6}/>
        </Responsive>
        <Cell fullHeight noSpacing size={12} mediumSize={6}>
          <div {...css(styles.content)}>
            <Spacing bottom={4} bottomMediumAndAbove={8} leftMediumAndAbove={8}>
              <Title inline short level={1} inverse>
                Uma nova forma de se relacionar.
              </Title>
              <Spacing bottom={1.5}>
                <Title level={3} light inverse>
                  Finalmente você pode realizar mais
                </Title>
              </Spacing>
              <Button primary tall wide onClick={() => navigate('/como-funciona')}>veja como funciona</Button>
            </Spacing>
          </div>
        </Cell>
      </Grid>
    </Wrapper>
  </div>
)

export default withStyles(({ color, units, breakpoint }) => ({
  cover: {
    backgroundImage: `url(${ coverImage })`,
    height: units(40),
    maxHeight: '100vh',
    backgroundSize: 'cover',
    [breakpoint.large]: {
      height: '100vh',
      minHeight: units(45),
      maxHeight: 'unset'
    }
  },

  content: {
    display: 'flex',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-start'
  },

  textHighlight: {
    color: color.primary
  }
}))(Cover)
