import React from 'react'
import { withStyles } from '../helpers/withStyles'

const TimelineItem = ({ children, css, lastItem, styles }) => (
  <div {...css(styles.container)}>
    <div {...css(styles.bulletArea)}>
      <div {...css(styles.bullet, lastItem && styles.bullet_lastItem)} />
      { !lastItem && <div {...css(styles.line)} /> }
    </div>
    <div {...css(styles.content)}>
      { children }
    </div>
  </div>
)

export default withStyles(({ breakpoint, color, units }) => ({
  container: {
    display: 'flex',
    alignItems: 'stretch',
    flexDirection: 'row'
  },

  bulletArea: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    paddingRight: units(3),
    transform: 'translateY(10px)',

    [breakpoint.large]: {
      paddingRight: units(5)
    }
  },

  bullet: {
    width: 26,
    height: 26,
    borderRadius: 13,
    background: color.grayDarker,
    //transform: 'scale(10%), translateY(50px)'
  },

  bullet_lastItem: {
    background: color.primary
  },

  line: {
    flex: 1,
    background: color.grayLight,
    width: 5
  },

  content: {
    paddingBottom: units(8)
  }
}))(TimelineItem)
