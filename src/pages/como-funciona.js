import React from 'react'
import Layout from '../components/layout'
import { Wrapper, Button, Title, Text, Grid, Cell, Spacing } from 'cinnecta-ui'
import { withStyles } from '../helpers/withStyles'
import TimelineItem from '../components/TimelineItem'
import comoFuncionaImage from '../images/como-funciona.jpg'

const ComoFuncionaPage = ({ css, styles }) => (
  <Layout headerPlaceholder withBackground backgroundColor="white">
    <div {...css(styles.cover)}>
      <Wrapper fullHeight container>
        <Grid fullHeight center>
          <Cell size={12} mediumSize={6} largeSize={5}>
            <Title inverse level={2}>Como funciona?</Title>
            <Text inverse large>Todas as informações de forma intuitiva e dinâmica, a qualquer momento e em qualquer lugar.</Text>
            <Spacing top={3}>
              <Button primary>Comece agora</Button>
              <Button inverse noSpacing>Veja o video</Button>
            </Spacing>
          </Cell>
        </Grid>
      </Wrapper>
    </div>
    <Wrapper container>
      <Spacing vertical={6}>
        <Grid>
          <Cell size={12} largeSize={8}>
            <TimelineItem>
              <Title mono level={3}>Contratação da plataforma</Title>
              <Text>Você consegue contratar a plataforma por aqui mesmo, na nossa pagina na web, é só clicar em COMECE AGORA</Text>
            </TimelineItem>
            <TimelineItem>
              <Title mono level={3}>Cadastre as suas principais informações</Title>
              <Text>Após seu registro e acesso liberados com sucesso é só cadastrar seus locais de atendimento, seus horários, os serviços e procedimentos que você realiza e algumas informações complementares.</Text>
            </TimelineItem>
            <TimelineItem>
              <Title mono level={3}>Baixe o App</Title>
              <Text>Baixe o app e acesse com seu login e senha</Text>
            </TimelineItem>
            <TimelineItem>
              <Title mono level={3}>Compartilhe com seus pacientes</Title>
              <Text>Agora em todos os momentos você já pode compartilhar o app com seus pacientes. Já no primeiro contato seja por telefone ou na consulta inaugural peça que ele baixe o app.</Text>
            </TimelineItem>
            <TimelineItem lastItem>
              <Title mono primary level={3}>Uma nova forma de se relacionar</Title>
              <Text>Todas as informações de forma intuitiva e dinâmica, a qualquer momento e em qualquer lugar.</Text>
            </TimelineItem>
          </Cell>
        </Grid>
      </Spacing>
    </Wrapper>
  </Layout>
)

export default withStyles(({ breakpoint, units }) => ({
  cover: {
    background: `url(${comoFuncionaImage})`,
    backgroundSize: 'cover',
    paddingTop: units(4),
    paddingBottom: units(4),
    display: 'flex',

    [breakpoint.large]: {
      height: 520
    }
  }
}))(ComoFuncionaPage)
