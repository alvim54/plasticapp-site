import React from 'react'
import Layout from '../components/layout'
import { Wrapper, Button, Title, Text, Grid, Cell, Spacing, Separator, Anchor } from 'cinnecta-ui'
import { withStyles } from '../helpers/withStyles'
import planoImage from '../images/plano.jpg'

const PlanoPage = ({ css, styles }) => (
  <Layout headerPlaceholder withBackground backgroundColor="white">
    <div {...css(styles.cover)}>
      <Wrapper fullHeight container>
        <Grid fullHeight right>
          <Cell size={12} mediumSize={6} largeSize={5}>
            <div {...css(styles.priceContainer)}>
              <Title level={3}>Um plano, um preço</Title>
              <Text small>Sem complicações, um só plano e todas as funcionalidades</Text>
              <Spacing vertical={2}>
                <Title level={2} primary>R$ 189/mês</Title>
              </Spacing>
              <Button primary wide tall>comece agora</Button>
              <Separator/>
              <Anchor external href="#faq">Ainda tem dúvidas? Veja nossas perguntas frequentes</Anchor>
            </div>
          </Cell>
        </Grid>
      </Wrapper>
    </div>
    <Wrapper id="faq" container>
      <Spacing vertical={6}>
        <Title primary level={3}>Perguntas frequentes:</Title>
        <Spacing vertical={3}>
          <Text tall bold large>1 - Qual a pergunta mais frequente sobre o aplicativo?</Text>
          <Text tall small>R: Todas as informações de forma intuitiva e dinâmica, a qualquer momento e em qualquer lugar. Otimização do tempo do profissional de saúde e de sua equipe integrando prontuario eletronico, agendamentos on line, prescrições médicas e todas informaçoes relativas ao paciente em uma só tela de apresentação. </Text>
        </Spacing>
        <Spacing vertical={3}>
          <Text tall bold large>2 - Qual a segunda pergunta mais frequente sobre o aplicativo?</Text>
          <Text tall small>R: Todas as informações de forma intuitiva e dinâmica, a qualquer momento e em qualquer lugar. Otimização do tempo do profissional de saúde e de sua equipe integrando prontuario eletronico, agendamentos on line, prescrições médicas e todas informaçoes relativas ao paciente em uma só tela de apresentação. </Text>
        </Spacing>
        <Spacing vertical={3}>
          <Text tall bold large>3 - Qual a terceira pergunta mais frequente sobre o aplicativo?</Text>
          <Text tall small>R: Todas as informações de forma intuitiva e dinâmica, a qualquer momento e em qualquer lugar. Otimização do tempo do profissional de saúde e de sua equipe integrando prontuario eletronico, agendamentos on line, prescrições médicas e todas informaçoes relativas ao paciente em uma só tela de apresentação. </Text>
        </Spacing>
      </Spacing>
    </Wrapper>
  </Layout>
)

export default withStyles(({ color, breakpoint, units }) => ({
  cover: {
    background: `url(${planoImage}) center`,
    backgroundSize: 'cover',
    paddingTop: units(15),
    display: 'flex',
    boxSizing: 'border-box',

    [breakpoint.large]: {
      height: 'calc(100vh - 108px)'
    }
  },

  priceContainer: {
    background: color.white,
    padding: units(2)
  }
}))(PlanoPage)
