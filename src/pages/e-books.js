import React from 'react'
import Layout from '../components/layout'
import { Wrapper, Title, Text, Grid, Cell, Spacing } from 'cinnecta-ui'
import Card from '../components/Card'
import { withStyles } from '../helpers/withStyles'
import comoFuncionaImage from '../images/como-funciona.jpg'

const EBooksPage = ({ css, styles }) => (
  <Layout headerPlaceholder withBackground backgroundColor="white">
    <div {...css(styles.cover)}>
      <Wrapper fullHeight container>
        <Grid fullHeight center>
          <Cell size={12} mediumSize={6} largeSize={5}>
            <Title inverse level={2}>Nossos e-books</Title>
            <Text inverse large>Conteúdo de qualidade produzido para as clínicas mais modernas</Text>
          </Cell>
        </Grid>
      </Wrapper>
    </div>
    <Wrapper container>
      <Spacing vertical={6}>
        <Grid>
          <Cell size={12} mediumSize={6} largeSize={3} largeSpacing>
            <Card
              title="Nome do e-book"
              description="Breve descrição do e-book"
              image="https://resultadosdigitais.com.br/blog/files/2011/07/como-fazer-um-ebook.jpg"
            />
          </Cell>
          <Cell size={12} mediumSize={6} largeSize={3} largeSpacing>
            <Card
              title="Nome do e-book"
              description="Breve descrição do e-book"
              image="https://resultadosdigitais.com.br/blog/files/2011/07/como-fazer-um-ebook.jpg"
            />
          </Cell>
          <Cell size={12} mediumSize={6} largeSize={3} largeSpacing>
            <Card
              title="Nome do e-book"
              description="Breve descrição do e-book"
              image="https://resultadosdigitais.com.br/blog/files/2011/07/como-fazer-um-ebook.jpg"
            />
          </Cell>
          <Cell size={12} mediumSize={6} largeSize={3} largeSpacing>
            <Card
              title="Nome do e-book"
              description="Breve descrição do e-book"
              image="https://resultadosdigitais.com.br/blog/files/2011/07/como-fazer-um-ebook.jpg"
            />
          </Cell>
          <Cell size={12} mediumSize={6} largeSize={3} largeSpacing>
            <Card
              title="Nome do e-book"
              description="Breve descrição do e-book"
              image="https://resultadosdigitais.com.br/blog/files/2011/07/como-fazer-um-ebook.jpg"
            />
          </Cell>
          <Cell size={12} mediumSize={6} largeSize={3} largeSpacing>
            <Card
              title="Nome do e-book"
              description="Breve descrição do e-book"
              image="https://resultadosdigitais.com.br/blog/files/2011/07/como-fazer-um-ebook.jpg"
            />
          </Cell>
          <Cell size={12} mediumSize={6} largeSize={3} largeSpacing>
            <Card
              title="Nome do e-book"
              description="Breve descrição do e-book"
              image="https://resultadosdigitais.com.br/blog/files/2011/07/como-fazer-um-ebook.jpg"
            />
          </Cell>
          <Cell size={12} mediumSize={6} largeSize={3} largeSpacing>
            <Card
              title="Nome do e-book"
              description="Breve descrição do e-book"
              image="https://resultadosdigitais.com.br/blog/files/2011/07/como-fazer-um-ebook.jpg"
            />
          </Cell>
        </Grid>
      </Spacing>
    </Wrapper>
  </Layout>
)

export default withStyles(({ breakpoint, units }) => ({
  cover: {
    background: `url(${comoFuncionaImage})`,
    backgroundSize: 'cover',
    paddingTop: units(4),
    paddingBottom: units(4),
    display: 'flex',
  }
}))(EBooksPage)
