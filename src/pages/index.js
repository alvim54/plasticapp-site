import React from 'react'
import Cover from '../components/Cover'
import { Wrapper, Grid, Cell, Spacing, Title, Text, Image, Responsive } from 'cinnecta-ui'
import timelineImage from '../images/home/timeline.jpg'
import timelineMockup from '../images/home/timeline-mockup.png'
import chatImage from '../images/home/chat.jpg'
import chatMockup from '../images/home/chat-mockup.png'
import analyticsImage from '../images/home/analytics.jpg'
import pagamentoImage from '../images/home/pagamento.jpg'
import marketingDigitalImage from '../images/home/marketing-digital.jpg'
import assistenciaMockup from '../images/home/assistencia-mockup.png'

import Layout from '../components/layout'
import { withStyles } from '../helpers/withStyles'

const IndexPage = ({ css, styles }) => (
  <Layout headerPlaceholder={false}>
    <Cover />
    <Spacing inside vertical={3} verticalMediumAndAbove={6}>
      <Wrapper container>
        <div {...css(styles.relative)}>
        <Grid center>
          <Cell size={12} mediumSize={6} largeSpacing>
            <Spacing bottom={2} bottomMediumAndAbove={0}>
              <Image
                aspectRatio={1.3}
                source={timelineImage}
                alt="Timeline"
                rectangular
              />
            </Spacing>
          </Cell>
          <Cell size={12} mediumSize={6} largeSpacing>
            <Title primary level={3}>Timeline</Title>
            <Spacing bottom={2}>
              <Title short mono level={1}>
                O controle de seus pacientes na palma da sua mão.
              </Title>
            </Spacing>
            <Text>
              Todas as informações de forma intuitiva e dinâmica,
              a qualquer momento e em qualquer lugar.
              Otimização do tempo do profissional de saúde e de sua equipe
              integrando prontuario eletronico, agendamentos on line,
              prescrições médicas e todas informaçoes relativas ao paciente
              em uma só tela de apresentação.
            </Text>
          </Cell>
        </Grid>
        <Responsive showFor="medium">
          <img {...css(styles.timelineMockup)} src={timelineMockup} alt="Tela Timeline" />
        </Responsive>
        </div>
      </Wrapper>
    </Spacing>
    <Spacing inside vertical={3} verticalMediumAndAbove={6}>
      <Wrapper container>
        <Grid center>
          <Cell size={12} mediumSize={6} largeSpacing>
            <Title primary level={3}>Chat online</Title>
            <Spacing bottom={2}>
              <Title short mono level={1}>
                Interação simples e direta, sem complicações.
              </Title>
            </Spacing>
            <Spacing inside rightMediumAndAbove={3}>
              <Text>
                Gerar aproximação entre o paciente e o profissional de saúde, permitindo contato frequente e não invasivo
                é um dos nossos focos. Nossa plataforma foi desenvolvida para substituir os canais de comunicação
                utilizados atualmente, como sms, email, telefone, redes sociais e aplicativos de mensagem.
                Queremos aproximar pessoas e melhorar a conexão entre elas, por meio do uso inteligente da tecnologia.
                Você próximo de seus pacientes de forma privada, organizada e profissional.
              </Text>
            </Spacing>
          </Cell>
          <Cell size={12} mediumSize={6}>
            <Spacing top={2} topMediumAndAbove={0}>
              <div {...css(styles.relative)}>
                <Image
                  aspectRatio={0.92}
                  source={chatImage}
                  alt="Chat"
                  rectangular
                />
                <img {...css(styles.chatMockup)} src={chatMockup} alt="Tela Chat" />
              </div>
            </Spacing>
          </Cell>
        </Grid>
      </Wrapper>
    </Spacing>
    <Spacing inside vertical={3} verticalMediumAndAbove={6}>
      <Wrapper container>
        <Grid center>
          <Cell size={12} mediumSize={6} largeSpacing>
            <Spacing bottom={2} bottomMediumAndAbove={0}>
              <Image
                source={analyticsImage}
                alt="Analytics"
                rectangular
              />
            </Spacing>
          </Cell>
          <Cell size={12} mediumSize={6} largeSpacing>
            <Title primary level={3}>Analytics</Title>
            <Spacing bottom={2}>
              <Title short mono level={1}>
                Desafiando a sua evolução.
              </Title>
            </Spacing>
            <Text>
              Apresenta visão geral do seu desempenho por meio de ferramentas com o foco no paciente. Métricas da sua
              interação com o paciente e da sua gestão financeira criam projeções de tendência e quantificam suas
              principais carências e necessidades.
            </Text>
          </Cell>
        </Grid>
      </Wrapper>
    </Spacing>
    <div {...css(styles.onlinePaymentContainer)} id="pagamento-online">
      <div {...css(styles.onlinePaymentContent)}>
        <Spacing inside vertical={3} verticalMediumAndAbove={6}>
          <div {...css(styles.relative)}>
            <Wrapper container>
              <Grid center>
                <Cell size={12} mediumSize={7} largeSpacing />
                <Cell size={12} mediumSize={5} largeSpacing>
                  <Spacing topLargeAndAbove={8}>
                    <Title primary level={3}>Pagamento online</Title>
                    <Spacing bottom={2}>
                      <Title short mono level={1}>
                        O fim da complexidade.
                      </Title>
                    </Spacing>
                    <Text>
                      Disponibilize em sua plataforma, de forma totalmente transparente, todas as formas de pagamento que
                      você trabalha. Foco no que importa, Você não precisa desenvolver uma infraestrutura de pagamentos para
                      o seu consultório – nós já cuidamos disso para você. Implemente pagamentos sem dor de cabeça.
                      Aceite pagamentos com boleto bancário, cartão de crédito e cheque através do nosso app.
                    </Text>
                  </Spacing>
                </Cell>
              </Grid>
            </Wrapper>
            <div {...css(styles.onlinePaymentImage)}>
              <Image
                aspectRatio={0.72}
                source={pagamentoImage}
                alt="Pagamento Online"
                rectangular
              />
            </div>
          </div>
        </Spacing>
      </div>
    </div>
    <Spacing inside vertical={3} verticalMediumAndAbove={6} topMediumAndAbove={18}>
      <Wrapper container>
        <Grid center>
          <Cell size={12} mediumSize={6} largeSpacing>
            <Title primary level={3}>Marketing digital</Title>
            <Spacing bottom={2}>
              <Title short mono level={1}>
                Digital agora é para todo mundo.
              </Title>
            </Spacing>
            <Text>
              Podemos otimizar seu marketing para te ajudar a conquistar novos pacientes, fidelizá-los e incentivar
              recomendações. Melhorando a sua presença digital para você usufruir os benefícios da visibilidade.
              Integramos suas postagens com o Facebook e Instagram e através delas você poderá ampliar seu
              relacionamento com seus pacientes: dispare mensagens, tire dúvidas e vire autoridade na sua especialidade.
            </Text>
          </Cell>
          <Cell size={12} mediumSize={6}>
            <Spacing top={2} topMediumAndAbove={0}>
              <Image
                source={marketingDigitalImage}
                alt="Marketing Digital"
                rectangular
              />
            </Spacing>
          </Cell>
        </Grid>
      </Wrapper>
    </Spacing>
    <Spacing inside vertical={3} verticalMediumAndAbove={6}>
      <Wrapper container>
        <Grid center>
          <Cell size={12} mediumSize={6} largeSpacing />
          <Cell size={12} mediumSize={6} largeSpacing>
            <Title primary level={3}>Assistência continuada</Title>
            <Spacing bottom={2}>
              <Title short mono level={1}>
                A tecnologia por um atendimento mais pessoal.
              </Title>
            </Spacing>
            <Text>
              Em casa, nosso aplicativo permite que o seu paciente se mantenha conectado com a equipe médica sempre que
              necessário e de forma dinâmica interage o paciente através de recomendações, conteúdos de relevância e
              monitoramento das datas ideais para as novas consultas, tudo através das ferramentas da plataforma.
            </Text>
          </Cell>
        </Grid>
      </Wrapper>
    </Spacing>
    <Spacing verticalMediumAndAbove={-15}>
      <Wrapper container>
        <Grid center>
          <Cell size={12}>
            <img
              style={{ width: '100%' }}
              src={assistenciaMockup}
              alt="Assistência continuada"
            />
          </Cell>
        </Grid>
      </Wrapper>
    </Spacing>
    <Spacing inside vertical={3} verticalMediumAndAbove={6}>
      <Wrapper container>
        <Grid center>
          <Cell size={12} mediumSize={6} largeSpacing>
            <Title primary level={3}>Segurança de dados</Title>
            <Spacing bottom={2}>
              <Title short mono level={1}>
                Prioridade máxima.
              </Title>
            </Spacing>
            <Text>
              Nos desenvolvemos todos os aspectos da nossa plataforma com segurança de dados em mente. Nossos processos
              de desenvolvimento foram orientados para assegurar os melhores níveis de segurança de dados possível, dando
              para médicos e pacientes o controle completo das informações armazenadas.
            </Text>
          </Cell>
          <Cell size={12} mediumSize={6} largeSpacing />
        </Grid>
      </Wrapper>
    </Spacing>
  </Layout>
)

export default withStyles(({ breakpoint, units }) => ({
  relative: {
    position: 'relative'
  },

  timelineMockup: {
    width: '30%',
    position: 'absolute',
    bottom: 0,
    left: 0,
    transform: 'translate(-40%, 20%)'
  },

  chatMockup: {
    width: '70%',
    position: 'absolute',
    bottom: 0,
    left: 0,
    transform: 'translate(-25%, 20%)'
  },

  onlinePaymentContainer: {
    [breakpoint.mediumAndAbove]: {
      width: '100%',
      height: 0,
      position: 'relative',
      paddingTop: '72%'
    }
  },

  onlinePaymentContent: {
    [breakpoint.mediumAndAbove]: {
      width: '100%',
      height: '100%',
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      position: 'absolute',
    }
  },

  onlinePaymentImage: {
    [breakpoint.mediumAndAbove]: {
      position: 'absolute',
      left: 0,
      top: 0,
      width: '100%',
      zIndex: -1
    }
  }
}))(IndexPage)
