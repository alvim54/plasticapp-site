import ProximaNovaFont from './ProximaNova-Regular.woff'
import ProximaNovaBoldFont from './ProximaNova-Bold.woff'

const unit = 16;
const columns = 12;
const globalRadius = 3;
const color = {
  primary: '#611A52',
  primarySaturated: '#610a4f',
  primaryDarker: '#4d1a3e',

  secondary: '#DA1C5C',
  secondarySaturated: '#d90b53',
  secondaryDarker: '#ac1c56',

  success: '#3adb76',
  warning: '#ffae00',
  alert: '#d93900',

  successLight: '',
  warningLight: '',
  alertLight: '#fff0e9',

  successDark: '',
  warningDark: '',
  alertDark: '#a52c00',

  gray: '#ececec',
  grayLighter: '#F2F3F4',
  grayLight: '#f5f5f5',
  grayDark: '#9B9B9B',
  grayDarker: '#4A5661',
  grayDarkest: '#2e2e2e',

  blueGray: '#e6e9f0',
  blueGrayLightest: '#F7F9FB',
  blueGrayLighter: '#f4f6fb',
  blueGrayLight: '#EEF0F4',
  blueGrayDark: '#8096b7',

  black: '#0a0a0a',
  white: '#fefefe',

  shadow: 'rgba(0, 0, 0, .2)',
  shadowWeakest: 'rgba(0, 0, 0, .035)',
  shadowWeaker: 'rgba(0, 0, 0, .055)',
  shadowWeak: 'rgba(0, 0, 0, .1)',
  shadowStrong: 'rgba(0, 0, 0, .3)',
  shadowStronger: 'rgba(0, 0, 0, .5)',
  shadowStrongest: 'rgba(0, 0, 0, .7)',

  fog: 'rgba(255, 255, 255, .2)',
  fogWeakest: 'rgba(255, 255, 255, .075)',
  fogWeaker: 'rgba(255, 255, 255, .1)',
  fogWeak: 'rgba(255, 255, 255, .15)',
  fogStrong: 'rgba(255, 255, 255, .3)',
  fogStronger: 'rgba(255, 255, 255, .5)',
  fogStrongest: 'rgba(255, 255, 255, .7)',

  transparent: 'transparent',
};

const input = {
  background: color.white,
  color: color.grayDarker,
  placeholderColor: color.grayDark,
  disabledBackground: color.gray,
  invalidBackground: color.alertLight,
  focusBackground: color.white,
  spacing: 0.5 * unit,

  border: {
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: color.grayLight,
    borderRadius: globalRadius
  },

  disabledBorder: {
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: color.gray,
    borderRadius: globalRadius
  },

  invalidBorder: {
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: color.alert,
    borderRadius: globalRadius
  },

  focusBorder: {
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: color.primary,
    borderRadius: globalRadius
  },

  radioBorderRadius: '50%',
  checkboxBorderRadius: globalRadius
};

const ProximaNova = [
  {
    fontFamily: "ProximaNova",
    fontStyle: "normal",
    fontWeight: "normal",
    src: `url(${ProximaNovaFont}) format('woff')`
  },
  {
    fontFamily: "ProximaNova",
    fontStyle: "normal",
    fontWeight: "bold",
    src: `url(${ProximaNovaBoldFont}) format('woff')`
  }
];

const font = {
  family: {
    default: [...ProximaNova, 'Oxygen, -apple-system, BlinkMacSystemFont, Roboto, "Helvetica Neue", sans-serif'],
    icon: 'cinnecta-suite'
  },

  size: {
    small: {
      title1: 32,
      title2: 26,
      title3: 21,
      title4: 18,
      title5: 21,
      title6: 21,
      large: 17,
      regular: 15,
      small: 14,
      micro: 10
    },

    large: {
      title1: 72,
      title2: 64,
      title3: 28,
      title4: 22,
      title5: 28,
      title6: 28,
      large: 24,
      regular: 20,
      small: 18,
      micro: 14
    },
  },
};

export default {
  unit: unit,
  units: (value) => value * unit,
  radius: globalRadius,
  columns: columns,

  icon: {
    defaultColor: color.grayDark,

    size: {
      micro: 8,
      smaller: 12,
      small: 16,
      default: 20,
      large: 24,
      huge: 36
    }
  },

  container: {
    micro: 800,
    small: 1200,
    default: 1440,
    large: 1800
  },

  weight: {
    light: 300,
    normal: 400,
    bold: 600
  },

  font: {
    ...font.family
  },

  button: {
    horizontalPadding: 1.25 * unit,
    compactHorizontalPadding: 0.75 * unit,
    wideHorizontalPadding: 1.75 * unit,
    verticalPadding: 0.25 * unit,
    borderWidth: 0,
    height: 2.5 * unit,
    shortHeight: 2 * unit,
    tallHeight: 3 * unit,
    radius: 50,
    fontSize: 16,
    fontSizeOnLarge: 17,
    largeFontSize: font.size.small.large,
    largeFontSizeOnLarge: font.size.large.large,
    smallFontSize: 15,
    smallFontSizeOnLarge: 15,
    spacing: 0.125 * unit,
    bottomSpacing: 0.5 * unit,
    fontWeight: 500,
    fontFamily: font.family.default,
  },

  input: {
    inlineSpacing: 32,
    spacing: input.spacing,
    borderRadius: globalRadius,
    background: input.background,
    color: input.color,
    placeholderColor: input.placeholderColor,
    verticalPadding: 10,
    horizontalPadding: 10,

    default: {
      ...input.border,
      background: input.background,
      color: input.color,
      fontFamily: font.family.default,
      fontSize: font.size.small.regular,
      letterSpacing: 'normal',
      lineHeight: 1.3,
      marginBottom: input.spacing,
    },

    disabled: {
      background: input.disabledBackground,
      ...input.disabledBorder
    },

    invalid: {
      background: input.invalidBackground,
      ...input.invalidBorder
    },

    small: {
      color: color.grayDarker,
      fontFamily: font.family.default,
      fontSize: font.size.small.small,
      letterSpacing: 'normal',
      lineHeight: 1.4,
    },

    focus: {
      ...input.focusBorder,
      background: input.focusBackground
    },

    radio: {
      size: 18,
      background: input.white,
      borderRadius: input.radioBorderRadius,

      border: {
        ...input.border,
        borderColor: color.gray,
        borderRadius: input.radioBorderRadius,
      },

      disabled: {
        background: input.disabledBackground,
        border: {
          ...input.disabledBorder,
          borderRadius: input.radioBorderRadius,
        }
      },

      invalid: {
        background: input.invalidBackground,
        border: {
          ...input.invalidBorder,
          borderRadius: input.radioBorderRadius,
        }
      },

      checkMark: {
        width: 8,
        height: 8,
        background: color.secondary,
        borderRadius: input.radioBorderRadius,
      }
    },

    checkbox: {
      size: 18,
      background: input.white,
      borderRadius: input.checkboxBorderRadius,

      border: {
        ...input.border,
        borderColor: color.gray,
        borderRadius: input.checkboxBorderRadius
      },

      disabled: {
        background: input.disabledBackground,
        border: {
          ...input.disabledBorder,
          borderRadius: input.checkboxBorderRadius
        }
      },

      invalid: {
        background: input.invalidBackground,
        border: {
          ...input.invalidBorder,
          borderRadius: input.checkboxBorderRadius
        }
      },

      checkMark: {
        width: 8,
        height: 8,
        background: color.secondary,
      }
    }
  },

  fontSize: {
    ...font.size
  },

  lineHeight: {
    regular: 1.4,
    short: 1.125,
    shorter: .9,
    tall: 1.6
  },

  shadow: {
    level0: { boxShadow: 'none' },
    level1: { boxShadow: '0 1px 2px 0 rgba(0, 0, 0, 0.05)' },
    level2: { boxShadow: '0 4px 4px 0 rgba(0, 0, 0, 0.05)' },
    level3: { boxShadow: '0 3px 9px 3px rgba(0, 0, 0, 0.05)' },
  },

  color: {
    ...color,

    shimmer: {
      from: color.blueGray,
      to: color.blueGrayLight
    },

    background: color.white
  },

  animation: {
    shimmer: {
      '0%': { backgroundColor: '#e6e9f0' },
      '50%': { backgroundColor: '#EEF0F4' },
      '100%': { backgroundColor: '#e6e9f0' }
    },

    spinner: {
      from: { transform: 'rotate(0deg)' },
      to: { transform: 'rotate(360deg)' }
    }
  },

  gutter: {
    small: {
      micro: {
        top: .25 * unit,
        right: .25 * unit,
        bottom: .25 * unit,
        left: .25 * unit,
      },

      small: {
        top: .5 * unit,
        right: .5 * unit,
        bottom: .5 * unit,
        left: .5 * unit,
      },

      regular: {
        top: 1 * unit,
        right: 1 * unit,
        bottom: 1 * unit,
        left: 1 * unit,
      },

      large: {
        top: 1 * unit,
        right: 1 * unit,
        bottom: 1 * unit,
        left: 1 * unit,
      },
    },

    medium: {
      micro: {
        top: .25 * unit,
        right: .25 * unit,
        bottom: .25 * unit,
        left: .25 * unit,
      },

      small: {
        top: .5 * unit,
        right: .5 * unit,
        bottom: .5 * unit,
        left: .5 * unit,
      },

      regular: {
        top: .5 * unit,
        right: .5 * unit,
        bottom: .5 * unit,
        left: .5 * unit,
      },

      large: {
        top: 1 * unit,
        right: 1 * unit,
        bottom: 1 * unit,
        left: 1 * unit,
      },
    },

    large: {
      micro: {
        top: .25 * unit,
        right: .25 * unit,
        bottom: .25 * unit,
        left: .25 * unit,
      },

      small: {
        top: .5 * unit,
        right: .5 * unit,
        bottom: .5 * unit,
        left: .5 * unit,
      },

      regular: {
        top: .5 * unit,
        right: .5 * unit,
        bottom: .5 * unit,
        left: .5 * unit,
      },

      large: {
        top: 3 * unit,
        right: 3 * unit,
        bottom: 3 * unit,
        left: 3 * unit,
      },
    },

    xlarge: {
      micro: {
        top: .25 * unit,
        right: .25 * unit,
        bottom: .25 * unit,
        left: .25 * unit,
      },

      small: {
        top: .5 * unit,
        right: .5 * unit,
        bottom: .5 * unit,
        left: .5 * unit,
      },

      regular: {
        top: 1 * unit,
        right: 1 * unit,
        bottom: 1 * unit,
        left: 1 * unit,
      },

      large: {
        top: 3 * unit,
        right: 3 * unit,
        bottom: 3 * unit,
        left: 3 * unit,
      },
    },
  }
};
