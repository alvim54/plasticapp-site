import ThemedStyleSheet from 'react-with-styles/lib/ThemedStyleSheet'
import aphroditeInterface from 'react-with-styles-interface-aphrodite'
import { css, withStyles } from 'react-with-styles'

import { Theme } from 'cinnecta-ui'
import PlasticAppTheme from '../PlasticAppTheme'

ThemedStyleSheet.registerTheme({
  ...Theme,
  ...PlasticAppTheme
})
ThemedStyleSheet.registerInterface(aphroditeInterface)

export { css, withStyles, ThemedStyleSheet }
